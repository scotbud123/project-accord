<?php
    session_start();
    
    //Include the smarty class
	include('./smarty-3.1.39/libs/Smarty.class.php');

    //Setup a default in-case something goes wrong
	$language = "english";
			
	$smarty = new Smarty; //Declare an instance of our Smarty object

    $smarty->display('index.tpl');

    session_destroy();
?>