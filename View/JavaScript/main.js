// jQuery Document
$(document).ready(function(){
	/*$("#testButton").click(function(){
        alert("Hello\nHow are you?");
    });*/

    //There is likely a better way to do this using jQuery specific code but, this vanilla JS will work for now

    /* 
    First, some constants are set up to reference the elements on the page to which we'll need access: the <video> into which the captured screen contents
    will be streamed, a box into which logged output will be drawn, and the start and stop buttons that will turn on and off capture of screen imagery.

    The object displayMediaOptions contains the MediaStreamConstraints to pass into getDisplayMedia(); here, the cursor property is set to always, indicating
    that the mouse cursor should always be included in the captured media.

    Finally, event listeners are established to detect user clicks on the start and stop buttons.
    */
    const videoElem = document.getElementById("video");
    const logElem = document.getElementById("log");
    const startElem = document.getElementById("start");
    const stopElem = document.getElementById("stop");

    // Options for getDisplayMedia()
    var displayMediaOptions = {
        video: {
            cursor: "always"
        },
        audio: false
    };

    // Set event listeners for the start and stop buttons
    startElem.addEventListener("click", function(evt) {
        startCapture(logElem, videoElem, displayMediaOptions);
    }, false);

    stopElem.addEventListener("click", function(evt) {
        stopCapture(videoElem);
    }, false);

    //To make logging of errors and other issues easy, this code overrides certain Console methods to output their messages to the <pre> block whose ID is log.
    console.log = msg => logElem.innerHTML += `${msg}<br>`;
    console.error = msg => logElem.innerHTML += `<span class="error">${msg}</span><br>`;
    console.warn = msg => logElem.innerHTML += `<span class="warn">${msg}<span><br>`;
    console.info = msg => logElem.innerHTML += `<span class="info">${msg}</span><br>`;
    //This allows us to use the familiar console.log(), console.error(), and so on to log information to the log box in the document.


});

//The startCapture() method, below, starts the capture of a MediaStream whose contents are taken from a user-selected area of the screen.
async function startCapture(logElem, videoElem, displayMediaOptions) {
    logElem.innerHTML = "";
  
    try {
      videoElem.srcObject = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
      dumpOptionsInfo(videoElem);
    } catch(err) {
      console.error("Error: " + err);
    }
}
/* 
After clearing the contents of the log in order to get rid of any leftover text from the previous attempt to connect,
startCapture() calls getDisplayMedia(), passing into it the constraints object defined by displayMediaOptions. Using
await, the following line of code does not get executed until after the promise returned by getDisplayMedia() resolves.
Upon resolution, the promise returns a MediaStream, which will stream the contents of the screen, window, or other region selected by the user.

The stream is connected to the <video> element by storing the returned MediaStream into the element's srcObject.

The dumpOptionsInfo() function—which we will look at in a moment—dumps information about the stream to the log box for educational purposes.

If any of that fails, the catch() clause outputs an error message to the log box.
*/

//---------------------------------------------------------------------------------------------------------------------------------------------

/* 
The stopCapture() method is called when the "Stop Capture" button is clicked. It stops the stream by getting its track list using
MediaStream.getTracks(), then calling each track's stop() method. Once that's done, srcObject is set to null to make sure it's
understood by anyone interested that there's no stream connected.
*/
function stopCapture(evt, videoElem) {
    let tracks = videoElem.srcObject.getTracks();
  
    tracks.forEach(track => track.stop());
    videoElem.srcObject = null;
}

/* 
For informational purposes, the startCapture() method shown above calls a method named dumpOptions(), which outputs the current
track settings as well as the constraints that were placed upon the stream when it was created.
*/
function dumpOptionsInfo(videoElem) {
    const videoTrack = videoElem.srcObject.getVideoTracks()[0];
  
    console.info("Track settings:");
    console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
    console.info("Track constraints:");
    console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
}
/* 
The track list is obtained by calling getVideoTracks() on the capture'd screen's MediaStream. The settings currently in effect
are obtained using getSettings() and the established constraints are gotten with getConstraints()
*/