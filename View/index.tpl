<!DOCTYPE html>
<!--Made by: Christo Mavrick-->
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Project Accord</title>
		<link rel="stylesheet" type="text/css" href="main.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script type="text/javascript" src="JavaScript/main.js" ></script>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Christo Mavrick">
		<meta name="description" content="TODO: Fill this in">
	</head>
	<body>
		<p>This example shows you the contents of the selected part of your display.
		Click the Start Capture button to begin.</p>
			
		<p><button id="start">Start Capture</button>&nbsp;<button id="stop">Stop Capture</button></p>
			
		<video id="video" autoplay></video>
		<br>
			
		<strong>Log:</strong>
		<br>
		<pre id="log"></pre>
		
		<footer>
			<div>
				<p>&copy; Copyright &copy; 2021. Christo Mavrick and Jonathan Theriault all rights reserved.</p>
			</div>
		</footer>
		</div>
		
		<!-- These divs are here to catch extra imagery -->
		<!--<div class="extra1" role="presentation"></div><div class="extra2" role="presentation"></div><div class="extra3" role="presentation"></div>
		<div class="extra4" role="presentation"></div><div class="extra5" role="presentation"></div><div class="extra6" role="presentation"></div> -->
	</body>
</html>